# Toolshed

<!-- MDTOC maxdepth:6 firsth1:1 numbering:0 flatten:0 bullets:1 updateOnSave:1 -->

- [Toolshed](#Toolshed)
  - [Why](#Why)
  - [About](#About)
  - [Usage](#Usage)
    - [Docker](#Docker)
    - [Kubernetes](#Kubernetes)
  - [TODO](#TODO)
  - [Utilities in This Image](#Utilities-in-This-Image)
    - [Network](#Network)
      - [`bind-tools`](#bind-tools)
      - [`cURL`](#cURL)
      - [`nmap`](#nmap)
      - [`net-tools`](#net-tools)
      - [`netcat-openbsd`](#netcat-openbsd)
      - [`tcpdump`](#tcpdump)
      - [`ngrep`](#ngrep)
    - [Clients](#Clients)
      - [`mysql-client`](#mysql-client)
      - [`redis`](#redis)
    - [Misc](#Misc)
      - [`bash`](#bash)
      - [`vim`](#vim)
      - [`coreutils`](#coreutils)
      - [`jq`](#jq)
      - [`ca-certificates`](#ca-certificates)

<!-- /MDTOC -->

---

+ [On GitLab](https://gitlab.com/chicken231/toolshed)
+ [On Docker Hub](https://hub.docker.com/r/chicken231/toolshed)

## Why

Can you always trust that convoluted application logs mean what they say? Want to make a **REST** call with `cURL` yourself? Want to check if an app is listening on a particular port `nc`? Want a lightweight container image that already has these tools installed?

Sometimes I just want to run a container on a server or in a cluster, and make sure things work as expected. But these things tools normally on an image. It's a pain to do `kubectl exec -ti <POD_NAME> -- sh`, then to run `apt update && apt install -y curl`. Of course you could combine these steps into one long command, but that's a long command to edit, and it takes time.

So I made my own image with the tools I want out-of-the-box, sans *Blackjack and Hookers*.

## About

+ This image is based on `alpine`, but with additional packages installed.
+ This `README` has some links to good resources about each tool.

## Usage

### Docker

```bash
docker run -ti chicken231/toolshed:latest
```

### Kubernetes

```bash
kubectl run -it --rm --restart=Never toolshed --image=chicken231/toolshed:latest --limits="memory=100Mi,cpu=100m"
```

The `ENTRYPOINT` is `bash`, so this is all you need is an interactive `tty`.

## TODO

+ Give example usage of various commands below.
+ Give an example of mounting a volume in Kubernetes to a `toolshed` pod.
+ Vet the links further.

## Utilities in This Image

### Network

#### `bind-tools`

+ Contains multiple tools like `nslookup`, `dig`, `host`, etc, for querying DNS.

#### `cURL`

+ [https://www.tecmint.com/linux-curl-command-examples/](https://www.tecmint.com/linux-curl-command-examples/)

#### `nmap`

+ [https://www.tecmint.com/nmap-command-examples/](https://www.tecmint.com/nmap-command-examples/)

#### `net-tools`

+ A package that contains multiple, common, network tools.

#### `netcat-openbsd`

+ [https://www.tecmint.com/check-remote-port-in-linux/](https://www.tecmint.com/check-remote-port-in-linux/)

#### `tcpdump`

+ [https://www.tecmint.com/12-tcpdump-commands-a-network-sniffer-tool/](https://www.tecmint.com/12-tcpdump-commands-a-network-sniffer-tool/)

#### `ngrep`

+ [https://www.tecmint.com/ngrep-network-packet-analyzer-for-linux/](https://www.tecmint.com/ngrep-network-packet-analyzer-for-linux/)

### Clients

#### `mysql-client`

+ [https://www.tecmint.com/gliding-through-database-mysql-in-a-nutshell-part-i/](https://www.tecmint.com/gliding-through-database-mysql-in-a-nutshell-part-i/)

#### `redis`

+ [https://redis.io/topics/rediscli](https://redis.io/topics/rediscli)

### Misc

#### `bash`

+ [http://www.bashoneliners.com/](http://www.bashoneliners.com/)

#### `vim`

+ [https://github.com/vim/vim](https://github.com/vim/vim)

#### `coreutils`

+ [https://www.gnu.org/software/coreutils/manual/coreutils.html](https://www.gnu.org/software/coreutils/manual/coreutils.html)

#### `jq`

+ [https://stedolan.github.io/jq/manual/](https://stedolan.github.io/jq/manual/)

#### `ca-certificates`

+ [https://en.wikipedia.org/wiki/Certificate_authority](https://en.wikipedia.org/wiki/Certificate_authority)
